"use strict";

module.exports = (app) => {
    let mongoose = require("mongoose");
    let logger = app.helpers.logger;
    let errorFormatter = app.helpers.errorFormatter;

    let cabSchema = mongoose.Schema({
        name: String,
        location: {
            type: [Number],
            index: '2d'
        },
        booked: Boolean,
        hipster: Boolean

    }, {
        timestamps: true,
        collection: "cabs"
    });

    let CabModel = mongoose.model("cab", cabSchema);

    CabModel.getAllCabs = () => {
        logger.info("Finding all cabs");
        return new Promise((resolve, reject) => {
            CabModel.find({}).then((cabs) => {
                logger.info("Cabs found - " + cabs.length);
                return resolve(cabs);
            }).catch((err) => {
                logger.error("Unable to find cabs");
                logger.error(err);
                let errorObject = errorFormatter.createErrorObject({
                    status: 404,
                    message: "Unable to find cabs",
                    details: err.message
                });
                return reject(errorObject);
            })
        })
    };

    CabModel.getUnbookedNearByCabs = (condition) => {
        logger.info("Finding cabs in near to location ");
       // var distance = 20/3959;

        //var location = condition.location;
        delete condition.location;
        condition.booked = false;
        return new Promise((resolve, reject) => {
            /*
             {location: {
             $near: [location.lng, location.lat], $maxDistance:distance
             }}
             */
            CabModel.find({}).where(condition)
                .lean()
                .exec()
                .then((cabs) => {
                    logger.info("Found cabs near to location " + cabs.length);
                    return resolve(cabs);
                })
                .catch((err) => {
                    logger.error("Unable to find near by cabs");
                    logger.error(err);
                    let errorObject = errorFormatter.createErrorObject({
                        status: 404,
                        message: "Unable to find near by cabs",
                        details: err.message
                    });
                    return reject(errorObject);
                })
        })
    };

    CabModel.addCab = (cabObj) => {
        return new Promise((resolve, reject) => {
            var cab = new CabModel(cabObj);
            cab.save(cabObj).then(cab => {
                logger.info("Successfully added cab");
                return resolve(cab.toJSON());
            }).catch(err => {
                logger.errro("Unable to add cab");
                logger.error(err);
                let errorObject = errorFormatter.createErrorObject({
                    status: 404,
                    message: "Unable to save cab",
                    details: err.message
                });
                return reject(errorObject);
            });
        })
    };

    CabModel.updateCab = (cabObj) => {
        logger.info("Updating cab")
        var id = cabObj._id;
        delete cabObj._id;
        return new Promise((resolve, reject) => {
            CabModel.findByIdAndUpdate(id, {$set: cabObj}, {new: true})
                .then((result) => {
                    logger.info("Updated cab to booked");
                    var cab = result.toJSON();
                    return resolve(cab);
                }).catch((err) => {
                    logger.error("Error in updating cab");
                    logger.error(err);
                    let errorObject = errorFormatter.createErrorObject({
                        status: 404,
                        message: "Unable to update cab",
                        details: err.message
                    });
                    return reject(errorObject);
                })
        })
    }

    return CabModel;
}
"use strict";

module.exports = (app) => {
    let mongoose = require("mongoose");
    let logger = app.helpers.logger;
    let errorFormatter = app.helpers.errorFormatter;
    let userSchema = mongoose.Schema({
        name: String,
        location:{
            type: [Number],
            index: '2d'
        },
        travelling: Boolean

    }, {
        timestamps: true,
        collection: "users"
    });

    let UserModel = mongoose.model("user", userSchema);

    UserModel.getAllUsers = () => {
        logger.info("Finding all users");
        return new Promise((resolve, reject) => {
            UserModel.find({}).then((users) => {
                logger.info("Users found - "+users.length);
                return resolve(users);
            }).catch((err) => {
                logger.error("Unable to find users");
                logger.error(err);
                let errorObject = errorFormatter.createErrorObject({
                    status: 404,
                    message: "Unable to find users",
                    details: err.message
                });
                return reject(errorObject);
            })
        })
    };
    
    UserModel.addUser = (userObj) => {
        return new Promise((resolve, reject) => {
            var user = new UserModel(userObj);
            user.save(userObj).then(user => {
                logger.info("Successfully added user");
                return resolve(user.toJSON());
            }).catch(err => {
                logger.error(err);
                let errorObject = errorFormatter.createErrorObject({
                    status: 404,
                    message: "Unable to save user",
                    details: err.message
                });
                return reject(errorObject);
            });
        })
    }

    return UserModel;
}
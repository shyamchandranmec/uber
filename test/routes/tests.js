
"use strict";

describe("Cabs service test", () => {

    
    it("Should create a cab", function (done) {
        let cab = {
            "name": "hi",
            "booked": true,
            "location": [28.5494489, 77.2001368],
            "hipster": true
        }
        request.post("/cabs")
            .send(cab)
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.be.json;
                expect(res.status).to.equal(200);
                expect(res.body).to.exist;
                expect(res.body._id).to.exist;
                done();
            });
    })

    it("Should not create a cab", function (done) {
        let cab = {
            "booked": true,
            "location": [28.5494489, 77.2001368],
            "hipster": true
        };
        request.post("/cabs")
            .send(cab)
            .end((err, res) => {
                expect(res.body).to.exist;
                expect(res.status).to.equal(400);
                expect(res.body.error).to.exist;
                expect(res.body.error).to.equal(true);
                done();
            });
    })

    it("Should book a cab", function (done) {
        let details = {
            "user": {
                "hipster": true,
                "location":{
                    "lat":11.2587531,
                    "lng":75.78041000000007
                }
            },
            "endLocation": {
                "lat":11.2587531,
                "lng":75.78041000000007
            }
        }
        request.post("/book-a-cab")
            .send(details)
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.be.json;
                expect(res.status).to.equal(200);
                expect(res.body).to.exist;
                expect(res.body._id).to.exist;
                done();
            });
    })


    it("Should  not book a cab", function (done) {
        let details = {

            "endLocation": {
                "lat":11.2587531,
                "lng":75.78041000000007
            }
        }
        request.post("/book-a-cab")
            .send(details)
            .end((err, res) => {
                expect(res.body).to.exist;
                expect(res.status).to.equal(400);
                expect(res.body.error).to.exist;
                expect(res.body.error).to.equal(true);
                done();
            });
    })

});


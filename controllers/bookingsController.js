

"use strict";

module.exports = (app) => {
    let bookingsService = app.services.bookingsService;

    let validator = app.validators.cabValidator;
    function requestACab(req, res, next) {
        let conf = {
            hipster: req.body.hipster,
            user: req.body.user,
            endLocation: req.body.endLocation
        };
        validator.validateRequestCab(conf).then((details) => {
                return bookingsService.requestACab(conf)
        }).then((cabs) => {
            res.json(cabs);
        }).catch((err) => {
            next(err);
        })
    }
    
    return {
        requestACab
    }
}
"use strict";

module.exports = (app) => {

    let cabsService = app.services.cabsService;
    let validator = app.validators.cabValidator;
    function getAllCabs (req, res, next) {
        return cabsService.getAllCabs().then((cabs) => {
            res.json(cabs);
        }).catch((err) => {
            next(err);
        })
    }

    function addCab (req, res, next) {
        if(!req.body.hipster) {
            req.body.hipster = false;
        }
        var cabObj = {
            name: req.body.name,
            location: req.body.location,
            booked: false,
            hipster: req.body.hipster
        };
        validator.validateCreateCab(cabObj).then((cab) => {
            return cabsService.addCab(cab)
        }).then((cab) => {
            res.json(cab);
        }).catch((err) => {
            next(err);
        })
    }

    function getUnbookedNearByCabs(req, res, next) {
        let conf = {
        };
        return cabsService.getUnbookedNearByCabs(conf).then((cabs) => {
            res.json(cabs);
        }).catch((err) => {
            next(err);
        })
    }

   

    return {
        getAllCabs,
        addCab,
        getUnbookedNearByCabs
    }
}
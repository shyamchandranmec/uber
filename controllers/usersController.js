"use strict";

module.exports = (app) => {

    let usersService = app.services.usersService;

    function getAllUsers (req, res, next) {
        return usersService.getAllUsers().then((users) => {
            res.json(users);
        }).catch((err) => {
            next(err);
        })
    }

    function addUser (req, res, next) {
        var userObj = {
            name: req.body.name,
            location: req.body.location,
            travelling: false
        };
        return usersService.addUser(userObj).then((user) => {
            res.json(user);
        }).catch((err) => {
            next(err);
        })
    }

    return {
        getAllUsers,
        addUser
    }
}
"use strict";

module.exports = app => {
    app.use("/", app.routes.index);
    app.use("/users", app.routes.users);
    app.use("/cabs", app.routes.cabs);
    app.use("/book-a-cab", app.routes.bookings);
};
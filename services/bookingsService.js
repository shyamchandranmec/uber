

"use strict";

module.exports = (app) => {

    let logger = app.helpers.logger;
    let errorFormatter = app.helpers.errorFormatter;
    let _ = require("underscore");
    function requestACab(conf) {
        let cabsService = app.services.cabsService;
        let cabConf = {
            location: conf.user.location,
            booked: false,
            hipster: conf.user.hipster
        };

        return new Promise((resolve, reject) => {
            cabsService.getUnbookedNearByCabs(cabConf)
                .then((cabs) => {
                    if(cabs.length == 0) {
                        logger.info("No unbooked cabs available");
                        let errorObject = errorFormatter.createErrorObject({
                            status: 404,
                            message: "No cabs available"
                        });
                        return reject(errorObject);
                    } else {
                        let selectedCab = findClosestCab(conf.user.location, cabs);
                        selectedCab.booked = true;
                        delete selectedCab.distance;
                        logger.info("Cab found");
                        cabsService.updateCab(selectedCab).then((cab) => {
                            logger.info("Updated cab booked  = true");
                            (function(cab, conf){
                                setTimeout(function () {
                                    let cabObj = {
                                        _id: cab._id,
                                        location: [conf.endLocation.lat, conf.endLocation.lng],
                                        booked: false
                                    };
                                    let totalDistanceTravelled = distance(conf.endLocation.lat, conf.endLocation.lng, conf.user.location.lat, conf.user.location.lng, 'M');

                                    cabsService.updateCab(cabObj).then((cab) => {
                                        logger.info("Cab arrived at users house "+totalDistanceTravelled);
                                        logger.info("User Travelled  "+totalDistanceTravelled+ "miles");
                                    })
                                },7000);
                            })(cab, conf);
                            return resolve (cab);

                        });

                    }

                })
        })
    }

    function findClosestCab(location, cabs) {
        var sortedCabs = [];
        let cab = null;
        for(var i = 0; i < cabs.length; i ++) {
            cab = cabs[i];
            cabs.distance = distance(location.lat, location.lng, cab.location[1], cab.location[0],'M')
        }
        sortedCabs = _.sortBy(cabs, 'distance');
        return sortedCabs[0];
    }

    function distance(lat1, lon1, lat2, lon2, unit) {
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180;
        if(radtheta == 0) {
            return 0;
        }
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return dist
    }

    return {
        requestACab
    };
}
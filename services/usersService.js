"use strict"

module.exports = (app) => {

    let userModel = app.models.user;
    
    function getAllUsers() {
        return userModel.getAllUsers();
    }

    function addUser(userObj) {
        return userModel.addUser(userObj);
    }


    return {
        getAllUsers,
        addUser
    }
}
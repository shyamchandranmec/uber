"use strict"

module.exports = (app) => {

    let cabModel = app.models.cab;
    
    function getAllCabs() {
        return cabModel.getAllCabs();
    }

    function addCab(cabObj) {
        return cabModel.addCab(cabObj);
    }

    function getUnbookedNearByCabs(conf) {
        return cabModel.getUnbookedNearByCabs(conf);
    }

    function updateCab(cabObj) {
        return cabModel.updateCab(cabObj);
    }


    return {
        getAllCabs,
        addCab,
        getUnbookedNearByCabs,
        updateCab
    }
}
"use strict"

module.exports = app => {

    let errorFormatter = app.helpers.errorFormatter;

    function validateCreateCab(cab) {
        return new Promise((resolve, reject) => {
            let errors = [];
            if(!cab.name) {
                errors.push("Name should not be empey")
            }
            if(cab.location.length != 2) {
                errors.push("Location should be an array");
            }

            let errorObject = errorFormatter.createErrorObject({
                status: 400,
                message: "Validation failed",
                details: errors
            });
            if(errors.length) {
                return reject(errorObject);
            } else {
                resolve(cab);
            }
        })
        
    }

    function validateRequestCab(details) {
        return new Promise((resolve, reject) => {
            let errors = [];
            try {
                if(!details.user) {
                    errors.push("Users details missing");
                }

                if(!details.user.location) {
                    errors.push("Users location details missing");
                }

                if(!details.endLocation) {
                    errors.push("Destination details missing");
                }
                if(!details.user.location.lat) {
                    errors.push("Users current latitude should not be empty")
                }
                if(!details.user.location.lng) {
                    errors.push("Users current lng should not be empty")
                }

                if(!details.endLocation.lat) {
                    errors.push("Users end location latitude should not be empty")
                }
                if(!details.endLocation.lng) {
                    errors.push("Users end location lng should not be empty")
                }

            } catch(e) {
                let errorObject = errorFormatter.createErrorObject({
                    status: 400,
                    message: "Validation failed",
                    details: errors
                });
                return reject(errorObject);

            }


            let errorObject = errorFormatter.createErrorObject({
                status: 400,
                message: "Validation failed",
                details: errors
            });
            if(errors.length) {
                return reject(errorObject);
            } else {
                resolve(details);
            }
        })

    }
    
    return  {
        validateCreateCab,
        validateRequestCab
    }
}
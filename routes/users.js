"use strict"

var express = require('express');
var router = express.Router();

module.exports = app => {
    let router = require("express").Router();
    let usersController = app.controllers.usersController;

    router.route('/')
        .get((req, res, next) => usersController.getAllUsers(req, res, next))
        .post((req, res, next) => usersController.addUser(req, res, next))

    return router;
}


"use strict"

var express = require('express');
var router = express.Router();

module.exports = app => {
    let router = require("express").Router();
    let cabsController = app.controllers.cabsController;

    router.route('/')
        .get((req, res, next) => cabsController.getAllCabs(req, res, next))
        .post((req, res, next) => cabsController.addCab(req, res, next));

    router.route('/unbooked')
        .get((req, res, next) => cabsController.getUnbookedNearByCabs(req, res, next))


    return router;
}


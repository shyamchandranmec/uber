

"use strict";


module.exports = app => {
    let router = require("express").Router();
    let bookingsController = app.controllers.bookingsController;

    router.route('/')
        .post((req, res, next) => bookingsController.requestACab (req, res, next))

    return router;
}
